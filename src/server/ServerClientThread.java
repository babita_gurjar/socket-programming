package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

import server_logic.ServerLogic;

import com.google.gson.Gson;

public class ServerClientThread extends Thread {
	Socket serverClient;
	int clientNo;
	static ServerLogic serverLogic;
	static Gson gsonObject;

	ServerClientThread(Socket inSocket, int counter) {
		serverClient = inSocket;
		clientNo = counter;
	}

	public void run() {
		try {
			processRequest(serverClient, clientNo);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void processRequest(Socket socket, int clientNo)
			throws Exception {
		String messageFromClient = "";
		Scanner scannerObject = new Scanner(System.in);
		serverLogic = new ServerLogic();
		DataInputStream inputStream = new DataInputStream(
				socket.getInputStream());
		DataOutputStream outputStream = new DataOutputStream(
				socket.getOutputStream());
		gsonObject = new Gson();

		while (!messageFromClient.equals("stop")) {
			messageFromClient = receiveResponse(messageFromClient, inputStream);
			sendResponse(messageFromClient, outputStream);
		}
		outputStream.writeUTF("stop");
		outputStream.flush();
		closeServer(scannerObject, outputStream, socket, clientNo);
	}

	public static String receiveResponse(String messageFromClient,
			DataInputStream inputStream) throws Exception {
		messageFromClient = (String) inputStream.readUTF();
		return messageFromClient;
	}

	public static void sendResponse(String messageFromClient,
			DataOutputStream outputStream) throws Exception {
		if (!messageFromClient.equals("stop")) {
			String commandResponse = " ";
			ServerLogic serverLogic = new ServerLogic();

			commandResponse = serverLogic
					.processClientMessage(messageFromClient);
			outputStream.writeUTF(commandResponse);
			outputStream.flush();
		}
	}

	public static void closeServer(Scanner scannerObject,
			DataOutputStream outputStream, Socket serverSocket, int clientNo)
			throws Exception {
		System.out.println("Client-" + clientNo + " exit!! ");
		outputStream.close();
		scannerObject.close();
		serverSocket.close();
	}
}
