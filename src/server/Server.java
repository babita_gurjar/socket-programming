package server;

import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

	static ServerSocket serverSocket;
	static Socket socket;
	static int counter = 0;

	public static void main(String args[]) throws Exception {
		try {
			startServer();
			acceptRequest();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void startServer() throws Exception {
		System.out.println("server started");
		serverSocket = new ServerSocket(8080);
		System.out.println("waiting for request");
	}

	public static void acceptRequest() throws Exception {
		while (true) {
			counter++;
			Socket serverClient = serverSocket.accept();
			System.out.println("Client No:" + counter + " connected!");
			ServerClientThread serverClientThread = new ServerClientThread(
					serverClient, counter); // send the request to a separate
											// thread
			serverClientThread.start();
		}
	}

}
