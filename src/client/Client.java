package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

import client_logic.ProcessUserInput;

public class Client {

	static DataOutputStream outputStream;
	static DataInputStream inputStream;
	static Scanner scannerObject;
	static Socket socket;

	public static void main(String[] args) throws Exception {
		try {
			requestToServer();
			communicationWithServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void requestToServer() throws Exception {
		socket = new Socket("localhost", 8080);
	}

	public static void communicationWithServer() throws Exception {
		String messageToServer = " ", messageFromServer = " ";
		scannerObject = new Scanner(System.in);
		outputStream = new DataOutputStream(socket.getOutputStream());
		inputStream = new DataInputStream(socket.getInputStream());
		System.out.println("request send to Server");

		while (!messageFromServer.equals("stop")) {
			sendResponse(messageToServer);
			messageFromServer = receiveResponse(messageFromServer);
		}
		closeConnection();
	}

	public static String receiveResponse(String messageFromServer)
			throws Exception {
		messageFromServer = (String) inputStream.readUTF();
		System.out.println(messageFromServer);
		return messageFromServer;
	}

	public static void sendResponse(String messageToServer) throws IOException {
		String[] input = new String[4];
		input = scannerObject.nextLine().split(" ");
		messageToServer = ProcessUserInput.processCommand(input);
		outputStream.writeUTF(messageToServer);
		outputStream.flush();
	}

	public static void closeConnection() throws Exception {
		System.out.println("client connection close");
		inputStream.close();
		outputStream.close();
		scannerObject.close();
		socket.close();
	}

}
