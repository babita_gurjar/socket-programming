package server_logic;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;

import org.junit.Test;

import pojo.UserInput;

import com.google.gson.Gson;

import server.Server;

public class ServerLogicTest {

	@Test
	public void testIsKeyPresent() throws IOException {
		Gson gsonObject = new Gson();
		ServerLogic serverLogic = new ServerLogic();
		String messageFromClient = "{ \"command\": \"" + "get"
				+ "\",\"key\":\"" + "vidushi" + "\",\"value\":\"" + "\"}";
		assertEquals("user name not present",
				serverLogic.processClientMessage(messageFromClient));

	}

	@Test
	public void testIsValidCommand() throws IOException {
		Gson gsonObject = new Gson();
		ServerLogic serverLogic = new ServerLogic();
		String messageFromClient = "{ \"command\": \"" + "ret"
				+ "\",\"key\":\"" + "Harshita" + "\",\"value\":\"" + "babita99"
				+ "\"}";
		assertEquals("Enter valid command",
				serverLogic.processClientMessage(messageFromClient));

	}
}
