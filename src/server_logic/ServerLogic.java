package server_logic;

import java.io.IOException;

import operations.HsetHgetOperation;
import operations.SetGetOperation;
import pojo.Command;
import pojo.Input;
import pojo.UserInput;

import com.google.gson.Gson;

public class ServerLogic {
	static Gson gsonObject = new Gson();
	static UserInput userInput;
	static Input input;
	static String commandResponse = " ";

	static ServerLogic serverLogic = new ServerLogic();
	static HsetHgetOperation hset_Hget = new HsetHgetOperation();
	static SetGetOperation set_Get = new SetGetOperation();

	public String processClientMessage(String messageFromClient)
			throws IOException {
		Command command = gsonObject.fromJson(messageFromClient, Command.class);
		serverLogic.requestObject(command, messageFromClient);
		return commandResponse;
	}

	public void requestObject(Command command, String messageFromClient)
			throws IOException {
		if (command.getCommand().equalsIgnoreCase("SET")
				|| command.getCommand().equalsIgnoreCase("GET")) {
			userInput = gsonObject.fromJson(messageFromClient, UserInput.class);
			commandResponse = set_Get.processCommand(command, userInput);
		} else if (command.getCommand().equalsIgnoreCase("HSET")
				|| command.getCommand().equalsIgnoreCase("HGET")) {
			input = gsonObject.fromJson(messageFromClient, Input.class);
			commandResponse = hset_Hget.processCommand(command, input);
		} else {
			commandResponse = "Enter valid command";
		}
	}

}
