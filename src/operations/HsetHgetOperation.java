package operations;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import pojo.Command;
import pojo.Input;

public class HsetHgetOperation {
	static String commandResponse;
	static Input input;

	private static ConcurrentHashMap<String, ConcurrentHashMap<String, String>> HRecord = new ConcurrentHashMap<String, ConcurrentHashMap<String, String>>();

	public String processCommand(Command command, Input input)
			throws IOException {
		HsetHgetOperation.input = input;
		if (command.getCommand().equalsIgnoreCase("HSET")) {
			hset();
		} else if (command.getCommand().equalsIgnoreCase("HGET")) {
			hget();

		}
		return commandResponse;
	}

	public static void hget() {
		if (HRecord.containsKey(input.getEntity())) {
			if (HRecord.get(input.getEntity()).containsKey(input.getProperty())) {
				commandResponse = HRecord.get(input.getEntity()).get(
						input.getProperty());
			} else {
				commandResponse = "field not Present";
			}
		} else {
			commandResponse = "Key is not present";

		}

	}

	public static void hset() {
		if (HRecord.containsKey(input.getEntity())) {
			if (HRecord.get(input.getEntity()).containsKey(input.getProperty())) {
				HRecord.get(input.getEntity()).replace(input.getProperty(),
						input.getValue());
				commandResponse = "field got updated";
			} else {
				HRecord.get(input.getEntity()).put(input.getProperty(),
						input.getValue());
				commandResponse = "field got added";
			}
		} else {
			ConcurrentHashMap<String, String> value = new ConcurrentHashMap<String, String>();
			value.put(input.getProperty(), input.getValue());
			HRecord.put(input.getEntity(), value);
			commandResponse = "ok";
		}

	}
}
