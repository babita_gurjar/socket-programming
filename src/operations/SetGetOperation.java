package operations;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import pojo.Command;
import pojo.UserInput;
import pojo.ValueObject;

public class SetGetOperation {
	static String commandResponse;
	static UserInput userInput;
	private static ConcurrentHashMap<String, ValueObject> record = new ConcurrentHashMap<String, ValueObject>();

	public String processCommand(Command command, UserInput userInput)
			throws IOException {
		SetGetOperation.userInput = userInput;
		if (command.getCommand().equalsIgnoreCase("set")) {
			commandResponse = record.containsKey(userInput.getKey()) ? updateData()
					: saveData();
		} else if (command.getCommand().equalsIgnoreCase("get")) {
			commandResponse = get(userInput.getKey());
		}
		return commandResponse;
	}

	public static String saveData() throws IOException {
		userInput.setExpiryTime((userInput.getExpiryTime() * 1000)
				+ System.currentTimeMillis());
		ValueObject valueObject = new ValueObject(userInput.getValue(),
				userInput.getExpiryTime());
		record.put(userInput.getKey(), valueObject);
		return "ok";
	}

	public static String updateData() {
		ValueObject valueObject = new ValueObject(userInput.getValue(),
				userInput.getExpiryTime());

		record.replace(userInput.getKey(), valueObject);
		return "Value got updated";

	}

	public static String get(String key) {
		if (record.containsKey(userInput.getKey())) {
			for (HashMap.Entry<String, ValueObject> entry : record.entrySet()) {
				if ((entry.getValue().getExpiryTime()) < System
						.currentTimeMillis()) {
					record.remove(entry.getKey());
				}
			}
			if (!record.containsKey(key)) {
				return "Key got Expire...";
			} else
				return record.get(key).getValue();
		} else {
			return "Key is not present";
		}
	}
}
