package client_logic;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProcessUserInputTest {

	@Test
	public void testProcessCommandIsStop() {
		String[] input = {"stop"};
		assertEquals("stop", ProcessUserInput.processCommand(input));

	}
	@Test
	public void testProcessCommandIsGet() {
		String[] input = {"get", "babita"};
		assertEquals( "{ \"command\": \"" + "get"
				+ "\",\"key\":\"" + "babita" + "\"}", ProcessUserInput.processCommand(input));

	}
	@Test
	public void testProcessCommandIsSet() {
		String[] input = {"set", "babita","babita99"};
		assertEquals("{ \"command\": \"" + "set"
				+ "\",\"key\":\"" + "babita" + "\",\"value\":\""
				+ "babita99" + "\"}", ProcessUserInput.processCommand(input));

	}

}
