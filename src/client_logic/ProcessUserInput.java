package client_logic;

public class ProcessUserInput {
	public static String processCommand(String[] input) {
		String messageToServer = " ";
		if (input.length == 1 && input[0].equalsIgnoreCase("stop")) {
			return input[0];
		} else if (input.length == 2 && input[0].equalsIgnoreCase("GET")) {
			messageToServer = "{ \"command\": \"" + input[0] + "\",\"key\":\""
					+ input[1] + "\"}";
		} else if (input.length == 4 && input[0].equalsIgnoreCase("SET")) {
			messageToServer = "{ \"command\": \"" + input[0] + "\",\"key\":\""
					+ input[1] + "\",\"value\":\"" + input[2]
					+ "\",\"expiryTime\":\"" + input[3] + "\"}";
		}else if (input.length == 4 && input[0].equalsIgnoreCase("HSET")) {
			messageToServer = "{ \"command\": \"" + input[0] + "\",\"entity\":\""
					+ input[1] + "\",\"property\":\"" + input[2]
					+ "\",\"value\":\"" + input[3] + "\"}";
		}else if (input.length == 3 && input[0].equalsIgnoreCase("HGET")) {
			messageToServer = "{ \"command\": \"" + input[0] + "\",\"entity\":\""
					+ input[1] + "\",\"property\":\"" + input[2] + "\"}";
		} else {
			messageToServer = "{ \"command\": \"" + "Invalid" + "\"}";
		}
		return messageToServer;
	}
}
