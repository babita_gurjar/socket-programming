package pojo;

public class ValueObject {
	private String value;
	private long expiryTime;
	public ValueObject(String value, long expiryTime) {
		this.value = value;
		this.expiryTime = expiryTime;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(long expiryTime) {
		this.expiryTime =  expiryTime;
	}
}
