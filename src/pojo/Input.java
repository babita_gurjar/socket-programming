package pojo;

public class Input {
	private String command;
	private String entity;
	private String property;
	private String value;

	public String getCommand() {
		return command;
	}

	public String getEntity() {
		return entity;
	}

	public String getProperty() {
		return property;
	}

	public String getValue() {
		return value;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
