package pojo;

public class UserInput {

	private String command;
	private String key;
	private String value;
	private long expiryTime;

	public UserInput(String command, String key, String value, long expiryTime) {
		this.command = command;
		this.key = key;
		this.value = value;
		this.expiryTime = expiryTime;
	}

	public String getCommand() {
		return command;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(long expiryTime) {
		this.expiryTime =  expiryTime;
	}
}
